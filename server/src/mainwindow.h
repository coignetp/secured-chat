#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>

#include "room.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void insertText(const QString &str);

public slots:
    void openPort(void);
    void closePort(void);

private:
    Ui::MainWindow *ui;
    QList<Room*> rooms_;
};

#endif // MAINWINDOW_H
