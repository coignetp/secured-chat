#ifndef ROOM_H
#define ROOM_H

#include <QTcpServer>
#include <QList>
#include <QObject>

#include "packet.h"

const int MAX_MESSAGE_LENGTH(1024);
const int MAX_CLIENT(128);

class Room : public QObject
{
    Q_OBJECT

public:
    Room(QObject *parent = 0);

    bool start(quint16 port);

    int getPort() const;
    QMap<quint32, QTcpSocket *> &getClients();

public slots:
    void newConnection();
    void receivePacket();
    void disconnectClient();
    void sendBroadcastMessage(const QString &message, const bool encrypted);
    void sendBroadcastInfoRoom();
    void sendBroadcast(Packet &packet);

private:
    quint16 port_;
    bool started_;
    QTcpServer *server_;
    quint32 maxId_;
    QMap<quint32, QTcpSocket *> clients_;
};

#endif // ROOM_H
