#ifndef PACKET_H
#define PACKET_H

#include <QBitArray>
#include <QByteArray>
#include <QString>
#include <QTcpSocket>

QByteArray IntToArray(qint32 source);
qint32 ArrayToInt(QByteArray source);
QByteArray bitsToBytes(QBitArray bits);
QBitArray bytesToBits(QByteArray bytes);

class Room;

class Packet
{
public:
    // On 4 bits
    enum PacketType {
        TEXT=0,
        ROOM_INFO,
        SEMI_KEY,
        TEXT_ENCRYPTED,
        UNKNOWN
    };

    Packet();
    Packet(const QString &str, const bool encrypted=false);
    Packet(const enum PacketType type);
    Packet(Room *room, const int first);

    bool send(QTcpSocket *socket);
    enum PacketType receive(QTcpSocket *socket);

    QString getMessage();

    quint32 getKeyDestination();

private:
    void writePacketType();
    void readPacketType();

    void writeString(QString str, int pos);
    void writeUInt8(const quint8, int pos);
    void writeUInt16(const quint16 n, int pos);
    void writeUInt32(const quint32 n, int pos);
    void writeNBits(int n, const int num, int pos);

    QString readString(int pos, int size);
    quint8 readUInt8(int pos);
    quint16 readUInt16(int pos);
    quint32 readUInt32(int pos);
    int readNBits(int n, int pos);

private:
    enum PacketType type_;
    QBitArray packet_;
    quint32 size_;
};

#endif // PACKET_H
