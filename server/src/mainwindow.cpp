#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QScrollBar>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::openPort(void) {
    for (auto it(rooms_.begin()) ; it != rooms_.end() ; it++) {
        auto p = (*it)->getPort();
        auto p2 = ui->spinBox->value();
        if (p2 == p)  {
            insertText("Port already opened");
            return;
        }
    }

    rooms_.push_back(new Room(this));
    if (!rooms_.back()->start(ui->spinBox->value())) {
        insertText("Error when opening a port\n");
    } else {
        insertText("New port opened: " + QString::number(ui->spinBox->value()));
    }
}

void MainWindow::insertText(const QString &str) {
    ui->textEdit->insertHtml(str + "<br>");
    ui->textEdit->verticalScrollBar()->setValue(ui->textEdit->verticalScrollBar()->maximum());
}

void MainWindow::closePort(void) {
    for (auto it(rooms_.begin()) ; it != rooms_.end() ; it++) {
        if (ui->spinBox->value() == (*it)->getPort()) {
            insertText("Port " + QString::number(ui->spinBox->value()) + " closed");
            (*it)->deleteLater();
            rooms_.erase(it);
            return;
        }
    }

    insertText("Port " + QString::number(ui->spinBox->value()) + " was not opened");
}
