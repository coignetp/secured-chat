#include "room.h"
#include "mainwindow.h"
#include "packet.h"

#include <QDataStream>
#include <QObject>
#include <QTcpSocket>


Room::Room(QObject *parent) : QObject(parent), started_(false), maxId_(0) {
}


bool Room::start(quint16 port) {
    if (port < 8000 || port > 10000)
        return false;

    server_ = new QTcpServer(this);
    if (!server_->listen(QHostAddress::Any, port))
    {
        return false;
    }

    connect(server_, SIGNAL(newConnection()), this, SLOT(newConnection()));
    port_ = port;

    return true;
}

int Room::getPort() const
{
    return port_;
}


QMap<quint32, QTcpSocket *> &Room::getClients() {
    return clients_;
}


void Room::newConnection() {
    sendBroadcastMessage("<em>A new client is connected</em>", false);
    qobject_cast<MainWindow*>(parent())->insertText("<em>A new client is connected on " + QString::number(port_) + "</em>");


    QTcpSocket *newClient = server_->nextPendingConnection();
    auto c = clients_.insert(++maxId_, newClient);

    qobject_cast<MainWindow*>(parent())->insertText("Id " + QString::number(c.key()));

    connect(newClient, SIGNAL(readyRead()), this, SLOT(receivePacket()));
    connect(newClient, SIGNAL(disconnected()), this, SLOT(disconnectClient()));

    sendBroadcastInfoRoom();
}

void Room::receivePacket() {
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    while(socket->bytesAvailable() > 0) {
        Packet p;
        Packet::PacketType type = p.receive(socket);

        if (type == Packet::TEXT || type == Packet::TEXT_ENCRYPTED) {
            QString message(p.getMessage());
            qobject_cast<MainWindow*>(parent())->insertText(message);

            sendBroadcastMessage(message, type == Packet::TEXT_ENCRYPTED);
        }
        else if (type == Packet::SEMI_KEY) {
            quint32 dest = p.getKeyDestination();
            if (dest != -1) {
                p.send(clients_[dest]);
            }
        }
    }
}

void Room::disconnectClient() {
    sendBroadcastMessage("<em>A client left the room</em>", false);
    qobject_cast<MainWindow*>(parent())->insertText("<em>A client left room " + QString::number(port_) + "</em>");

    // On d�termine quel client se d�connecte
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) // Si par hasard on n'a pas trouv� le client � l'origine du signal, on arr�te la m�thode
        return;

    for (auto it = clients_.begin() ; it  != clients_.end();) {
        if (it.value() == socket)
            it = clients_.erase(it);
        else
            ++it;
    }

    socket->deleteLater();
}

void Room::sendBroadcastMessage(const QString &message, const bool encrypted) {
   Packet p(message, encrypted);
   sendBroadcast(p);
}

void Room::sendBroadcastInfoRoom() {
    for (auto client: clients_.toStdMap()) {
        Packet p(this, client.first);
        p.send(client.second);
    }
}

void Room::sendBroadcast(Packet &packet) {
    for (auto client: clients_.toStdMap()) {
        packet.send(client.second);
    }
}
