#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "textencryption.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool hasConnection(const QHostAddress &senderIp, int senderPort) const;
    void insertText(const QString &str);

    void sendSemiKeyToNext(quint64 modKey, quint32 numOfParticipant);
    quint32 getNextParticipant();

public slots:
    void sendPacket();
    void askConnection();
    void receivePacket();
    void connect();
    void disconnect();
    void socketError(QAbstractSocket::SocketError erreur);

private:
    TextEncryption textEncryption_;
    Ui::MainWindow *ui;
    QString name_;
    QTcpSocket *socket_;
    QList<int> participants_;
    quint32 participantsShared_;
    quint32 nextParticipant_;
    int id_;
};

#endif // MAINWINDOW_H
