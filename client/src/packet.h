#ifndef PACKET_H
#define PACKET_H

#include <QBitArray>
#include <QByteArray>
#include <QString>
#include <QTcpSocket>

QByteArray IntToArray(qint32 source);
qint32 ArrayToInt(QByteArray source);
QByteArray bitsToBytes(QBitArray bits);
QBitArray bytesToBits(QByteArray bytes);

class Packet
{
public:
    // On 4 bits
    enum PacketType {
        TEXT=0,
        ROOM_INFO,
        SEMI_KEY,
        TEXT_ENCRYPTED,
        UNKNOWN
    };

    Packet();
    Packet(const QString &str, const bool encrypted=false);
    Packet(const quint64 modKey, const quint32 numberOfParticipant, const quint32 nextParticipant);
    Packet(const enum PacketType type);

    bool send(QTcpSocket *socket);
    enum PacketType receive(QTcpSocket *socket);

    QString getMessage();
    QList<int> getClientList();

    quint32 getKeyDestination();
    quint32 getParticipantsShared();
    quint64 getSemiKey();

private:
    void writePacketType();
    void readPacketType();

    void writeString(QString str, int pos);
    void writeUInt8(const quint8, int pos);
    void writeUInt16(const quint16 n, int pos);
    void writeUInt32(const quint32 n, int pos);
    void writeUInt64(const quint64 n, int pos);
    void writeNBits(int n, const int num, int pos);

    QString readString(int pos, int size);
    quint8 readUInt8(int pos);
    quint16 readUInt16(int pos);
    quint32 readUInt32(int pos);
    quint64 readUInt64(int pos);
    int readNBits(int n, int pos);

private:
    enum PacketType type_;
    QBitArray packet_;
    quint32 size_;
};

#endif // PACKET_H
