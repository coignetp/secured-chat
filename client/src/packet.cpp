#include <QDataStream>
#include <QException>

#include "packet.h"

const int PACKET_TYPE_BITS = 8;
const int PACKET_SIZE_BITS = 32;

QByteArray IntToArray(qint32 source) //Use qint32 to ensure that the number have 4 bytes
{
    //Avoid use of cast, this is the Qt way to serialize objects
    QByteArray temp;
    QDataStream data(&temp, QIODevice::ReadWrite);
    data << source;
    return temp;
}

qint32 ArrayToInt(QByteArray source)
{
    qint32 temp;
    QDataStream data(&source, QIODevice::ReadWrite);
    data >> temp;
    return temp;
}

QByteArray bitsToBytes(QBitArray bits) {
    QByteArray bytes;
    bytes.resize(bits.count()/8+ (bits.count() % 8 == 0 ? 0 : 1));
    bytes.fill(0);
    // Convert from QBitArray to QByteArray
    for(int b=0; b<bits.count(); ++b)
        bytes[b/8] = ( bytes.at(b/8) | ((bits[b]?1:0)<<(7 - b%8)));
    return bytes;
}

QBitArray bytesToBits(QByteArray bytes) {
    QBitArray bits;
    bits.resize(bytes.count() * 8);
    bits.fill(0);
    // Convert from QByteArray to QBitArray
    for(int b=0; b<bits.count(); ++b)
        bits[b] = ((bytes[b/8] >> (7 - b%8)) & 0x1);
    return bits;
}

Packet::Packet() {
    type_ = PacketType::UNKNOWN;
}

Packet::Packet(const QString &str, const bool encrypted)
{
    type_ = (encrypted ? PacketType::TEXT_ENCRYPTED : PacketType::TEXT);
    packet_.resize(PACKET_TYPE_BITS + PACKET_SIZE_BITS + (str.size()) * 8);
    writePacketType();
    writeUInt32(str.size(), PACKET_TYPE_BITS);
    writeString(str, PACKET_TYPE_BITS + PACKET_SIZE_BITS);

    size_ = str.size();
}

//  1 2 3 4 5 6 7 8
// |      TYPE     |
// |   PKT  SIZE   |
// |      ..       |
// |      ..       |
// |      ..       |
// |      NEXT     |
// |      ..       |
// |      ..       |
// |      ..       |
// |      NB       |
// |      ..       |
// |      ..       |
// |      ..       |
// |      KEY      |
// |      ..       |
// |      ..       |
// |      ..       |
// |      ..       |
// |      ..       |
// |      ..       |
// |      ..       |


Packet::Packet(const quint64 modKey, const quint32 numberOfParticipant, const quint32 nextParticipant) {
    type_ = PacketType::SEMI_KEY;
    packet_.resize(PACKET_TYPE_BITS + PACKET_SIZE_BITS + 32 + 32 + 64);

    size_ = 4 + 4 + 8;

    writePacketType();
    writeUInt32(size_, PACKET_TYPE_BITS);
    writeUInt32(nextParticipant, PACKET_TYPE_BITS + PACKET_SIZE_BITS);
    writeUInt32(numberOfParticipant, PACKET_TYPE_BITS + PACKET_SIZE_BITS + 32);
    writeUInt64(modKey, PACKET_TYPE_BITS + PACKET_SIZE_BITS + 32 + 32);

}

Packet::Packet(const enum PacketType type) {
    if (type < 0 || type >= PacketType::UNKNOWN)
        throw QException();
    type_ = type;
    writePacketType();
}


bool Packet::send(QTcpSocket *socket) {
    if (socket == nullptr)
        return false;

    auto bytes = bitsToBytes(packet_);
    int totalSize = size_ + (PACKET_SIZE_BITS + PACKET_TYPE_BITS) / 8;

    if (bytes.size() != totalSize)
        throw QException();

    socket->write(bytes, totalSize);

    return true;
}

Packet::PacketType Packet::receive(QTcpSocket *socket) {
    QByteArray buffer;

    if (socket->bytesAvailable() < 1 + (PACKET_TYPE_BITS + PACKET_SIZE_BITS) / 8)
         return PacketType::UNKNOWN;

    buffer.append(socket->read((PACKET_TYPE_BITS + PACKET_SIZE_BITS) / 8));

    packet_ = bytesToBits(buffer);
    readPacketType();
    size_ = readUInt32(PACKET_TYPE_BITS);

    if (socket->bytesAvailable() < size_)
        throw QException();

    buffer.append(socket->read(size_));
    packet_ = bytesToBits(buffer);

    int test = socket->bytesAvailable();

    return type_;
}


QString Packet::getMessage() {
    if (type_ == PacketType::TEXT || type_ == PacketType::TEXT_ENCRYPTED) {
        return readString(PACKET_TYPE_BITS + PACKET_SIZE_BITS, readUInt32(PACKET_TYPE_BITS));
    }
    return "";
}


QList<int> Packet::getClientList() {
    if (type_ != PacketType::ROOM_INFO)
        throw QException();

    QList<int> list;
    int size = readUInt16(PACKET_TYPE_BITS + PACKET_SIZE_BITS);

    if (size <= 0)
        throw QException();

    for (int i(0) ; i < size ; i++) {
        list.push_back(readUInt32(PACKET_TYPE_BITS + PACKET_SIZE_BITS + 16 + 8 * 4 * i));
    }

    return list;
}

quint32 Packet::getKeyDestination() {
    if (type_ == PacketType::SEMI_KEY) {
        return readUInt32(PACKET_TYPE_BITS + PACKET_SIZE_BITS);
    }
    return -1;
}

quint32 Packet::getParticipantsShared() {
    if (type_ == PacketType::SEMI_KEY) {
        return readUInt32(PACKET_TYPE_BITS + PACKET_SIZE_BITS + 32);
    }

    return -1;
}

quint64 Packet::getSemiKey() {
    if (type_ == PacketType::SEMI_KEY) {
        return readUInt64(PACKET_TYPE_BITS + PACKET_SIZE_BITS + 32 + 32);
    }

    return -1;
}

void Packet::writePacketType() {
    writeNBits(PACKET_TYPE_BITS, type_, 0);
}

void Packet::readPacketType() {
    type_ = static_cast<PacketType>(readNBits(PACKET_TYPE_BITS, 0));
}


void Packet::writeString(QString str, int pos) {
    for (int i(0) ; i < str.size() ; i++) {
        auto a = str.at(i).cell();
        writeUInt8(a, pos + i*8);
    }
}

void Packet::writeUInt8(const quint8 n, int pos) {
    int bitNumber = sizeof(n) * 8;
    quint8 ncopy = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = (ncopy % (1 << i) != ncopy);

        packet_.setBit(pos + (bitNumber - i - 1), bit);

        if (bit) {
            ncopy -= (1 << i);
        }

    }
}

void Packet::writeUInt16(const quint16 n, int pos) {
    int bitNumber = sizeof(n) * 8;
    quint16 ncopy = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = (ncopy % (1 << i) != ncopy);

        packet_.setBit(pos + (bitNumber - i - 1), bit);

        if (bit) {
            ncopy -= (1 << i);
        }

    }
}

void Packet::writeUInt32(const quint32 n, int pos) {
    int bitNumber = sizeof(n) * 8;
    quint32 ncopy = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = (ncopy % (1 << i) != ncopy);

        packet_.setBit(pos + (bitNumber - i - 1), bit);

        if (bit) {
            ncopy -= (1 << i);
        }

    }
}

void Packet::writeUInt64(const quint64 n, int pos) {
    int bitNumber = sizeof(n) * 8;
    quint64 ncopy = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = (ncopy % (1 << i) != ncopy);

        packet_.setBit(pos + (bitNumber - i - 1), bit);

        if (bit) {
            ncopy -= (1 << i);
        }

    }
}

void Packet::writeNBits(int nbit, const int n, int pos) {
    int bitNumber = nbit;
    quint32 ncopy = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = (ncopy % (1 << i) != ncopy);

        packet_.setBit(pos + (bitNumber - i - 1), bit);

        if (bit) {
            ncopy -= (1 << i);
        }

    }
}


QString Packet::readString(int pos, int size) {
    QString str;

    for (int i(0) ; i < size ; i++) {
        str.append(readUInt8(pos + i * 8));
    }

    return str;
}

quint8 Packet::readUInt8(int pos) {
    quint8 n = 0;
    int bitNumber = sizeof(n) * 8;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = packet_.at(pos + (bitNumber - i - 1));

        if (bit) {
            n += (1 << i);
        }
    }

    return n;
}

quint16 Packet::readUInt16(int pos) {
    quint16 n = 0;
    int bitNumber = sizeof(n) * 8;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = packet_.at(pos + (bitNumber - i - 1));

        if (bit) {
            n += (1 << i);
        }
    }

    return n;
}

quint32 Packet::readUInt32(int pos) {
    quint32 n = 0;
    int bitNumber = sizeof(n) * 8;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = packet_.at(pos + (bitNumber - i - 1));

        if (bit) {
            n += (1 << i);
        }
    }

    return n;
}

quint64 Packet::readUInt64(int pos) {
    quint64 n = 0;
    int bitNumber = sizeof(n) * 8;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = packet_.at(pos + (bitNumber - i - 1));

        if (bit) {
            n += (1 << i);
        }
    }

    return n;
}

int Packet::readNBits(int n, int pos) {
    int num = 0;
    int bitNumber = n;

    for (int i(bitNumber-1) ; i >= 0 ; i--) {
        bool bit = packet_.at(pos + (bitNumber - i - 1));

        if (bit) {
            num += (1 << i);
        }
    }

    return num;
}
