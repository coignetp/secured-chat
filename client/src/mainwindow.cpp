#include <QException>
#include <QScrollBar>
#include <QTcpSocket>

#include "mainwindow.h"
#include "packet.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), name_("noname"), id_(0) {
  ui->setupUi(this);

  socket_ = new QTcpSocket(this);
  QObject::connect(socket_, SIGNAL(readyRead()), this, SLOT(receivePacket()));
  QObject::connect(socket_, SIGNAL(connected()), this, SLOT(connect()));
  QObject::connect(socket_, SIGNAL(disconnected()), this, SLOT(disconnect()));
  QObject::connect(socket_, SIGNAL(error(QAbstractSocket::SocketError)), this,
                   SLOT(socketError(QAbstractSocket::SocketError)));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::sendPacket() {
  if (ui->lineEditMessage->text().size() > 0) {
    // On pr�pare le paquet � envoyerother
    QString messageToSend =
        textEncryption_.encrypte("<strong>" + ui->lineEditName->text() +
                                 "</strong> : " + ui->lineEditMessage->text());

    Packet p(messageToSend, true);
    p.send(socket_);

    ui->lineEditMessage->clear();     // On vide la zone d'�criture du message
    ui->lineEditMessage->setFocus();  // Et on remet le curseur � l'int�rieur
  }
}

void MainWindow::askConnection() {
  // On annonce sur la fen�tre qu'on est en train de se connecter
  insertText("<em>Try to connect..</em>");
  ui->pushButtonConnect->setEnabled(false);

  socket_->abort();  // On d�sactive les connexions pr�c�dentes s'il y en a
  socket_->connectToHost(
      ui->lineEdit->text(),
      ui->spinBox->value());  // On se connecte au serveur demand�
}

void MainWindow::connect() {
  insertText("<em>Connection succed</em>");
  ui->pushButtonConnect->setEnabled(false);
  ui->pushButtonSend->setEnabled(true);
}

// Ce slot est appel� lorsqu'on est d�connect� du serveur
void MainWindow::disconnect() {
  insertText("<em>Disconnected</em>");
  ui->pushButtonConnect->setEnabled(true);
  ui->pushButtonSend->setEnabled(false);
}

void MainWindow::socketError(QAbstractSocket::SocketError erreur) {
  switch (erreur)  // On affiche un message diff�rent selon l'erreur qu'on nous
                   // indique
  {
    case QAbstractSocket::HostNotFoundError:
      insertText("ERROR : server not found. Please check the IP qnd the port");
      break;
    case QAbstractSocket::ConnectionRefusedError:
      insertText("ERROR : connection refused");
      break;
    case QAbstractSocket::RemoteHostClosedError:
      insertText("ERROR : connection closed by server");
      break;
    default:
      insertText("ERROR : " + socket_->errorString());
  }

  ui->pushButtonConnect->setEnabled(true);
}

void MainWindow::receivePacket() {
  quint64 modKey;
  while (socket_->bytesAvailable() > 0) {
    Packet p;

    Packet::PacketType type = p.receive(socket_);

    switch (type) {
      case Packet::PacketType::TEXT:
        insertText(p.getMessage());
        break;
      case Packet::PacketType::ROOM_INFO:
        participants_ = p.getClientList();
        id_ = participants_.front();
        insertText("<em>" + QString::number(participants_.size()) +
                   " client(s) in the room / " + QString::number(id_));

        if (participants_.count() > 1) {
          ui->pushButtonSend->setEnabled(false);
          modKey = textEncryption_.createOwnSemiKey();
          participantsShared_ = 1;
          nextParticipant_ = getNextParticipant();
          participantsShared_ = 1;
          sendSemiKeyToNext(modKey, 1);

          insertText("<em>My semi key is " + QString::number(modKey) + "</em>");
        }
        break;
      case Packet::PacketType::SEMI_KEY:
        if (p.getParticipantsShared() != participantsShared_)
          throw QException();

        participantsShared_++;

        if (participantsShared_ == participants_.count()) {
          insertText("<em>Encryption ready</em>");
          textEncryption_.addLastKey(p.getSemiKey());
          ui->pushButtonSend->setEnabled(true);
        } else {
          sendSemiKeyToNext(textEncryption_.addSemiKey(p.getSemiKey()),
                            participantsShared_);
        }
        break;
      case Packet::PacketType::TEXT_ENCRYPTED:
        insertText(textEncryption_.decrypte(p.getMessage()));
        break;
      default:
        throw QException();
    }
  }
}

quint32 MainWindow::getNextParticipant() {
  if (participants_.count() <= 1) return -1;

  for (auto it : participants_) {
    if (it > id_) return it;
  }

  return participants_.at(1);  // .front();
}

void MainWindow::sendSemiKeyToNext(quint64 modKey, quint32 numOfParticipant) {
  //    insertText("Send semi key " + QString::number(modKey) + " to n " +
  //    QString::number(nextParticipant_));

  Packet p(modKey, numOfParticipant, nextParticipant_);
  p.send(socket_);
}

void MainWindow::insertText(const QString &str) {
  ui->textEdit->moveCursor(QTextCursor::End);
  ui->textEdit->insertHtml(str + "<br>");
  ui->textEdit->verticalScrollBar()->setValue(
      ui->textEdit->verticalScrollBar()->maximum());
}
