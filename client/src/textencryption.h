#ifndef TESTENCRYPTION_H
#define TESTENCRYPTION_H

#include <QString>

#include "simplecrypt.h"

std::string uInt64ToString(quint64 n);

class TextEncryption
{
public:
    TextEncryption();

    QString encrypte(QString str);
    QString decrypte(QString str);

    quint64 createOwnSemiKey();

    quint64 addSemiKey(quint64 semiKey) const;
    void addLastKey(quint64 semiKey);
private:
    bool init_;
    quint64 semiKey_;
    quint64 totalKey_;
    SimpleCrypt crypt_;
};

#endif // TESTENCRYPTION_H
