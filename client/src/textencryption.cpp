#include "textencryption.h"
#include "biginteger.h"

#include <QtMath>
#include <random>
#include <string>

const quint64 DIFFIE_HELLMAN_P = 839;
const std::string DIFFIE_HELLMAN_P_BIG = uInt64ToString(DIFFIE_HELLMAN_P);
const quint64 DIFFIE_HELLMAN_G = 349;

std::string uInt64ToString(quint64 n) {
  std::string str("00000000000000000000");

  for (int i(0); i < str.size(); i++) {
    str[str.size() - 1 - i] = (n % 10) + '0';
    n /= 10;
  }

  return str;
}

BigInteger bigIntPower(const quint64 n, quint64 power) {
  BigInteger copy(1);
  BigInteger nBig(uInt64ToString(n));

  for (quint64 i(0); i < power; i++) {
    copy *= nBig;
  }

  return copy;
}

quint64 stringToUInt64(const std::string &str) {
  quint64 n(0);

  quint64 p(1);
  for (int i(str.size() - 1); i >= 0; i--) {
    n += ((str[i] - '0') * p);
    p *= 10;
  }

  return n;
}

TextEncryption::TextEncryption() : init_(false) {}

QString TextEncryption::encrypte(QString str) {
  if (!init_) return str;

  return crypt_.encryptToString(str);
}

QString TextEncryption::decrypte(QString str) {
  if (!init_) return str;

  return crypt_.decryptToString(str);
}

quint64 TextEncryption::createOwnSemiKey() {
  std::random_device
      rd;  // Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd());  // Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dis(1, DIFFIE_HELLMAN_P);

  semiKey_ = dis(gen);

  BigInteger bigint(bigIntPower(DIFFIE_HELLMAN_G, semiKey_));

  bigint = bigint % BigInteger(DIFFIE_HELLMAN_P_BIG);

  return stringToUInt64(bigint.getNumber());
}

quint64 TextEncryption::addSemiKey(quint64 semiKey) const {
  BigInteger bigint(bigIntPower(semiKey, semiKey_));

  bigint = bigint % BigInteger(DIFFIE_HELLMAN_P_BIG);

  return stringToUInt64(bigint.getNumber());
}

void TextEncryption::addLastKey(quint64 semiKey) {
  totalKey_ = quint64(qPow(semiKey, semiKey_)) % DIFFIE_HELLMAN_P;

  crypt_.setKey(totalKey_);

  init_ = true;
}
